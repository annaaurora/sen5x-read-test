with (import <nixpkgs> { });

rustPlatform.buildRustPackage rec {
  name = "sen5x-read-test";

  src = ./.;
  driver = fetchFromGitHub {
    owner = "Sensirion";
    repo = "raspberry-pi-i2c-sen5x";
    rev = "ac9081e48bb584031ac73541f7dec2e18e519329";
    hash = "sha256-Igc/RvVk+DbDZk/CsgokCyp8Hz2QS6ytFOmzAy+37Ow=";
  };

  postPatch = ''
    cp ${driver}/sen* ./
  '';

  cargoHash = "sha256-pBaS9sgAdcvnGIg1nvAuleHbYPqIQ0RibP43DhnPUS0=";

  nativeBuildInputs = [ rustPlatform.bindgenHook glibc ];
  buildInputs = [ glibc ];
}
