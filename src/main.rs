#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

use std::{thread::sleep, time::Duration};

fn main() {
    let mut error: i16 = 0;
    
    unsafe { sensirion_i2c_hal_init() }

    error = unsafe { sen5x_device_reset() };
    if error != 0 {
        eprintln!("Error executing reset: {}", error);
    }

    let temp_offset = 0.0;
    error = unsafe { sen5x_set_temperature_offset_simple(temp_offset) };
    if error != 0 {
        eprintln!("Error setting temperature offset: {}", error);
    }
    
    error = unsafe { sen5x_start_measurement() };
    if error != 0 {
        panic!("Error starting measurement: {}", error);
    }

    // To prevent values that are 0 but not a measurement.
    let mut ready: bool = false;
    eprintln!("Waiting for measurements to be ready.");
    while ready == false {
        error = unsafe { sen5x_read_data_ready(&mut ready) };
        if error != 0 {
            panic!("Error checking if measurements are ready: {}", error);
        }

        if ready == false {
            let fifty_millis = Duration::from_millis(50);
            sleep(fifty_millis);
        }
    }
    eprintln!("Ready.");

    for _i in 0..600 {
        // Sleep for 1s. It's about the time the sensor takes to have new measurements.
        unsafe { sensirion_i2c_hal_sleep_usec(1000000) };

        let mut mass_concentration_pm1p0: f32 = 0.0;
        let mut mass_concentration_pm2p5: f32 = 0.0;
        let mut mass_concentration_pm4p0: f32 = 0.0;
        let mut mass_concentration_pm10p0: f32 = 0.0;
        let mut ambient_humidity: f32 = 0.0;
        let mut ambient_temperature: f32 = 0.0;
        let mut voc_index: f32 = 0.0;
        let mut nox_index: f32 = 0.0;

        error = unsafe { sen5x_read_measured_values(
            &mut mass_concentration_pm1p0,
            &mut mass_concentration_pm2p5,
            &mut mass_concentration_pm4p0,
            &mut mass_concentration_pm10p0,
            &mut ambient_humidity,
            &mut ambient_temperature,
            &mut voc_index,
            &mut nox_index
        ) };
        if error != 0 {
            eprintln!("Error reading measurements: {}", error);
        } else {
            println!("PM1.0: {:.}µg/m³", mass_concentration_pm1p0);
            println!("PM2.5: {:.}µg/m³", mass_concentration_pm2p5);
            println!("PM4.0: {:.}µg/m³", mass_concentration_pm4p0);
            println!("PM10.0: {:.}µg/m³", mass_concentration_pm10p0);
            println!("Ambient humidity: {:.}%RH", ambient_humidity);
            println!("Ambient temperature: {:.}°C", ambient_temperature);
            println!("VOC index: {:.}", voc_index);
            println!("NOₓ index: {:.}", nox_index);
        }
    }

    error = unsafe { sen5x_stop_measurement() };
    if error != 0 {
        panic!("Error stopping measurement: {}", error);
    }
}
